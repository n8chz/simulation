#!/usr/bin/python3

# Exercise 4-4 (not Model 4-4)

import simpy
import randomvariates
from math import ceil, floor, log10

rv = randomvariates.random.RandomVariates()

class Part(simpy.Process):
    def __init__(self, env, part_type):
        self.env = env # TODO but do I need this?
        self.type = part_type
        self.arrival_time = env.now

    def get_processed(self):

class Facility:
    def __init__(self):
        self.env = simpy.Environment()

    def part_1_arrival(self):
        while True:
            part = Part(self.env, 1)
            self.env.process(part.get_processed(self))
            yield self.env.timeout(rv.lognormal(mu=11.5, sd=2)[0])

    def part_2_arrival(self):
        while True:
            part = Part(self.env, 1)
            self.env.process(part.get_processed(self))
            yield self.env.timeout(rv.exponential(1/15.1)[0])


