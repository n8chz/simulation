#!/usr/local/bin/python3

# Exercise 3-3 (not Model 3-3)

import simpy
import randomvariates
from itertools import product
from math import ceil, floor, log10


rv = randomvariates.random.RandomVariates()

class Part(simpy.Process):
    def __init__(self, dps):
        self.dps = dps
        self.env = dps.env
        self.time = {'arrival': self.env.now}

    def go_through_station(self, resource, delay):
        req = resource.request()
        resource.q[self.env.now] = len(resource.queue)
        yield req
        start = self.env.now
        resource.b[self.env.now] = resource.count
        yield self.env.timeout(delay)
        resource.q[self.env.now] = len(resource.queue)
        self.time[resource] = (start, self.env.now)
        yield resource.release(req)
        resource.b[self.env.now] = resource.count

    def do_things(self):
        yield self.env.process(self.go_through_station(self.dps.drill_press, rv.triangular(1, 3, 6)[0]))
        yield self.env.process(self.go_through_station(self.dps.washer, rv.triangular(1, 3, 6)[0]))
        yield self.env.process(self.go_through_station(self.dps.inspector, 4.5/60))


class DrillPressStation:
    def __init__(self, time=480):
        self.time = time
        self.env = simpy.Environment()
        self.drill_press = simpy.Resource(self.env, capacity=1) 
        self.drill_press.q = {}
        self.drill_press.b = {}
        self.washer = simpy.Resource(self.env, capacity=1) 
        self.washer.q = {}
        self.washer.b = {}
        self.inspector = simpy.Resource(self.env, capacity=1) 
        self.inspector.q = {}
        self.inspector.b = {}
        self.parts = []
        self.env.process(self.part_arrival())
        self.env.run(until=time)
        self.display_data()

    def part_arrival(self):
        while True:
            part = Part(self)
            self.parts.append(part)
            self.env.process(part.do_things())
            yield self.env.timeout(rv.exponential(1/5)[0])

    @staticmethod
    def normalize(extent, level, max_level, reverse=False):
        if level == 0 and max_level == 0: # kludge
            level = 1
            max_level = 1
        if reverse:
            return 0.95*extent-0.9*extent*level/max_level
        else:
            return 0.05*extent+0.9*extent*level/max_level

    def queue_svg(self, seq, width=987, height=610):
        max_t = max(seq)
        max_val = max(seq.values())
        level = 0
        x = self.normalize(width, 0, self.time)
        y = self.normalize(height, 0, max_val, True)
        points = '{:f},{:f}'.format(x, y)
        for t in sorted(seq):
            new_level = seq[t]
            if new_level != level:
                x = self.normalize(width, t, self.time)
                y1 = self.normalize(height, level, max_val, True)
                y2 = self.normalize(height, new_level, max_val, True)
                points += ' {:f},{:f} {:f},{:f}'.format(x, y1, x, y2)
                level = new_level
        svg = '<svg viewBox="0 0 {:d} {:d}" xmlns="http://www.w3.org/2000/svg">'.format(width, height)
        svg += '<polyline fill="none" stroke="black" points="{:s}" />'.format(points)
        svg += self.queue_axes(seq, width, height)
        svg += '</svg>'
        return svg

    @staticmethod
    def autorange(lo, hi, ticks):
        approx = (hi-lo)/ticks
        oom = 10**floor(log10(approx)) # order of magnitude
        cand = [oom*x for x in [1, 2, 5]]
        score = [abs(log10(c/approx)) for c in cand]
        return cand[score.index(min(score))]



    def queue_axes(self, seq, width=987, height=610):
        max_t = int(max(seq))
        max_val = max(list(seq.values())+[1])
        t_inc = self.autorange(0, max_t, 13)
        val_inc = max(self.autorange(0, max_val, 8), 1)
        axes = ''
        y = 0.975*height
        for t in range(0, max_t, t_inc):
            x = self.normalize(width, t, max_t)
            axes += '<text x={:f} y={:f}>{:d}</text>'.format(x, y, t)
        return axes # TODO: also do y axis



    def display_data(self):
        for seq in [self.drill_press, self.washer, self.inspector]:
            print(self.queue_svg(seq.q))
            print(self.queue_svg(seq.b))

DrillPressStation(480)
