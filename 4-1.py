#!/usr/bin/python3

# Simulation problem in Figure 4-1 on page 122 of "Simulation with Arena"
# by W. David Kelton, Randall P. Sadowski, and Nancy B. Zupick,
# 6th edition, 2014, McGraw Hill

import simpy
import randomvariates

rv = randomvariates.random.RandomVariates()

class Part(simpy.Process):
    def __init__(self, env, part_type):
        self.type = part_type
        self.arrival_time = env.now

    def get_processed(self, eats):
        res = eats.prep[self.type]
        req = res.request()
        yield req
        eats.q_max[self.type] = max(eats.q_max[self.type], len(res.queue))
        self.process = dict()
        self.process['start'] = eats.env.now
        yield eats.env.timeout(rv.triangular(res.lo, res.mode, res.hi)[0])
        self.process['end'] = eats.env.now
        yield res.release(req)
        eats.env.process(self.get_sealed(eats))

    def get_sealed(self, eats):
        if self.type == 'a':
            seal_time = rv.triangular(1, 3, 4)[0]
        elif self.type == 'b':
            seal_time = rv.weibull(2.5, 5.3)[0]
        else:
            raise Exception('{:s} is not a valid part type'.format(self.type))
        res = eats.sealer
        req = res.request()
        yield req
        eats.q_max['seal'] = max(eats.q_max['seal'], len(res.queue))
        self.seal = dict()
        self.seal['start'] = eats.env.now
        yield eats.env.timeout(seal_time)
        self.seal['end'] = eats.env.now
        yield res.release(req)
        if rv.uniform(0, 100)[0] < 9:
            eats.env.process(self.get_reworked(eats))
        else:
            self.get_shipped()

    def get_reworked(self, eats):
        res = eats.rework
        req = res.request()
        yield req
        eats.q_max['rework'] = max(eats.q_max['rework'], len(res.queue))
        self.rework = dict()
        self.rework['start'] = eats.env.now
        yield eats.env.timeout(rv.exponential(1/45)[0])
        self.rework['end'] = eats.env.now
        yield res.release(req)
        if rv.uniform(0, 100)[0] > 20:
            self.get_shipped()
        else:
            self.get_scrapped()

    def get_shipped(self):
        print('shipped {:s}'.format(self.stats()))

    def get_scrapped(self):
        print('scrapped {:s}'.format(self.stats()))

    def stats(self):
        if self.seal['start']-self.process['end'] > 0.0: self.type = self.type.upper()
        history = [
            self.type,
            self.arrival_time,
            self.process['start'],
            self.process['end'],
            self.seal['start'],
            self.seal['end']
        ]
        if hasattr(self, 'rework'):
            history.append(self.rework['start'])
            history.append(self.rework['end'])
        return str(history)


    



class Eats: # electronic assembly and test system
    def __init__(self):
        self.env = simpy.Environment()
        self.prep = dict()
        for part_type, lo, mode, hi in [['a', 1, 4, 8], ['b', 3, 5, 10]]:
            self.prep[part_type] = simpy.Resource(self.env, capacity=1)
            self.prep[part_type].lo = lo
            self.prep[part_type].mode = mode
            self.prep[part_type].hi = hi
        self.sealer = simpy.Resource(self.env, capacity=1)
        self.rework = simpy.Resource(self.env, capacity=1)
        self.q_max = {'a': 0, 'b': 0, 'seal': 0, 'rework': 0}

        self.env.process(self.part_a_arrival())
        self.env.process(self.part_b_arrival())
        self.env.run(until=1920)
        print('maximum queue lengths during run: {:s}'.format(str(self.q_max)))

    def part_a_arrival(self):
        while True:
            part = Part(self.env, 'a')
            self.env.process(part.get_processed(self))
            yield self.env.timeout(rv.exponential(1/5)[0])

    def part_b_arrival(self):
        while True:
            parts = [Part(self.env, 'b') for _ in range(4)]
            [self.env.process(part.get_processed(self)) for part in parts]
            yield self.env.timeout(rv.exponential(1/30)[0])

Eats()
