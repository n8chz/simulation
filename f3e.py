#!/usr/bin/python3

import simpy

class DrillPressStation:
    def __init__(self, run_time=20):
        self.iat = [2.60, 1.35, 0.71, 0.62, 14.28, 0.70, 15.52, 3.15, 1.76, 1.00]
        self.st = [2.90, 1.76, 3.39, 4.52, 4.46, 4.36, 2.07, 3.36, 2.37, 5.38]
        self.env = simpy.Environment()
        self.drill_press = simpy.Resource(self.env, capacity=1)
        self.queue = []
        self.p = 0 # parts produced
        self.n = 0
        self.sum_wq = 0
        self.max_wq = 0
        self.sum_ts = 0
        self.max_ts = 0
        self.q_int = 0.0
        self.max_q = 0
        self.b_time = 0.0
        self.b_int = 0.0
        self.q = 0 # queue length
        self.q_time = 0.0
        self.q_change = 0.0
        self.report("init")
        self.env.process(self.work_piece())
        self.env.run(until=run_time)
        self.q_int += max(0, len(self.queue)-1)*(self.env.now-self.q_change)
        if self.drill_press.count:
            self.b_int += self.env.now-self.q_change
        self.report("end")

    def report(self, event_type):
        # Columns to print for each row:
        # entity no.
        # time
        # event type (init, arr, dep)
        # Q(t)
        # B(t)
        # (In Queue) by arrival time
        # in service (one entity, by arrival time)
        # p
        # N
        # \sum{WQ}
        # WQ^*
        # \sum{TS}
        # \int{Q}
        # Q^*
        # \int{B}
        # 3 rows of Event Calendar
        if event_type == "arr":
            entity = self.queue[-1][0]
            self.b_int += self.drill_press.count*(self.env.now-self.b_time)
        elif event_type == "dep":
            self.b_int += self.env.now-self.b_time
            entity = self.queue[0][0]
            self.q_int += max(0, len(self.queue)-1)*(self.env.now-self.q_change)
            # print("\n{:5.2f} {:5.2f}".format(self.env.now, self.q_int))
            self.queue.pop()
            self.q_change = self.env.now
        else:
            entity = None
        self.b_time = self.env.now
        if entity:
            print("{:2d}".format(int(entity)), end=" ")
        else:
            print(" -", end=" ")
        print("{:5.2f}".format(self.env.now), end=" ")
        print("{:4s}".format(event_type), end=" ")
        print("{:2d}".format(max(0, len(self.queue)-1)), end=" ")
        b = self.drill_press.count
        if event_type == "dep" and self.queue: b = 1
        print("{:2d}".format(b), end=" ")
        queue_rep = str(list(map(lambda x: round(x[1], 2), self.queue[:-1])))
        print("{:24s}".format(queue_rep), end=" ")
        # in service
        if self.queue:
            print("{:5.2f}".format(self.queue[-1][1]), end=" ")
        else:
            print("  -  ", end=" ")
        print("{:2d}".format(self.p), end=" ")
        print("{:2d}".format(self.n), end=" ")
        print("{:5.2f}".format(self.sum_wq), end=" ")
        print("{:5.2f}".format(self.max_wq), end=" ")
        print("{:5.2f}".format(self.sum_ts), end=" ")
        print("{:5.2f}".format(self.max_ts), end=" ")
        # TODO last 3 columns are all wrong, pls fix
        print("{:5.2f}".format(self.q_int), end=" ")
        print("{:2d}".format(self.max_q), end=" ")
        print("{:5.2f}".format(self.b_int))


    def drilling(self):
        while self.drill_press.count < self.drill_press.capacity and self.queue:
            i, at = self.queue[-1]
            # print("{:5.2f} Start drilling work piece {:d}".format(self.env.now, i))
            now = self.env.now
            wq = now-at
            self.sum_wq += wq
            if wq > self.max_wq: self.max_wq = wq
            # self.q_int += (now-self.q_time)*self.q
            self.q = len(self.queue)
            self.q_time = now
            # self.report()
            req = self.drill_press.request()
            st = self.st.pop(0)
            self.b_time = now
            yield self.env.timeout(st)
            self.drill_press.release(req)
            self.p += 1
            ts = self.env.now-at
            self.sum_ts += ts
            if ts > self.max_ts: self.max_ts = ts
            # self.b_int +=  st
            # print("{:5.2f} Done drilling work piece {:d}".format(self.env.now, i))
            self.report("dep")

    def work_piece(self):
        while self.iat:
            self.q_int += max(0, len(self.queue)-1)*(self.env.now-self.q_change)
            # print("\n{:5.2f} {:5.2f}".format(self.env.now, self.q_int))
            self.queue.insert(0, (self.n+1, self.env.now))
            self.max_q = max(self.max_q, len(self.queue)-1)
            self.q_change = self.env.now
            i = self.iat.pop(0)
            # print("{:5.2f} Piece {:d} placed in queue".format(self.env.now, self.n+1))
            # print("{:5.2f} Queue: {:s}".format(self.env.now, str(list(map(lambda x: x[0], self.queue)))))
            self.report("arr")
            self.env.process(self.drilling())
            yield self.env.timeout(i)
            self.n += 1

DrillPressStation(10)
