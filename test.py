#!/usr/bin/python3

import simpy

env = simpy.Environment()

def foo():
    print(str(env.now))
    yield env.timeout(2.2)
    print(str(env.now))

env.process(foo())
env.run(until=2.3)
