#!/usr/bin/python3

import sys
import simpy
import randomvariates
from math import log

rv = randomvariates.random.RandomVariates()

def hms(t): # audience-friendly display of simulation time values
    h, ms = divmod(t+7.5, 1) # assume start time of 07:30
    m, s = divmod(round(3600*ms), 60)
    return '{:02d}:{:02d}:{:02d}'.format(round(h), m, s)

class Round:
    def __init__(self, expected_attendance=5000, n_buses=100, hours=11, bus_time_interval=0.15):
        self.env = simpy.Environment()
        self.att = expected_attendance
        self.hours = hours
        self.lot = simpy.Resource(self.env)
        self.lot.waiting = 0
        self.lot.time_ref = 0
        self.lot.exp_ar = lambda t: abs(2*self.att*(1-t/self.hours)*(t < self.hours))
        self.lot.desc = "parking lot"
        self.lot.m = 10/60
        self.course = simpy.Resource(self.env)
        self.course.waiting = 0
        self.course.exp_ar = lambda t: (t < self.hours)*self.ppl_on_course*(1-t/self.hours)/2+5*(t >= self.hours)*self.ppl_on_course
        self.course.desc = "golf course"
        self.course.m = 11/60
        self.nbus = n_buses
        self.time_interval = bus_time_interval
        self.ppl_on_course = 0
        print('About to bring in the buses.')
        self.buses = [self.env.process(Bus(self, seq).deploy()) for seq in range(self.nbus)]
        self.buses_out = self.nbus
        self.log = []
        self.env.run(until=24-7.5) # TODO research exit conditions other than "until"

    def stats(self):
        # utilization stats for buses
        drive_time = 0
        run_time = 0
        total_passengers = 0
        for run in self.log:
            drive_time += run['disembark']-run['depart']
            run_time += run['disembark']-run['start_boarding']
            total_passengers += run['passengers']
        print('Buses are in motion {:6.2f}% of the time'.format(100*drive_time/run_time))
        print('Average number of passengers per bus run is {:5.2f}'.format(total_passengers/len(self.log)))


class Bus(simpy.Process):
    def __init__(self, sim, seq, capacity=40):
        self.id = seq
        self.sim = sim
        self.env = sim.env
        self.passengers = 0
        self.capacity = capacity
        print('Bus #{:d} ready for event.'.format(self.id))

    def deploy(self):
        # print('Starting bus #{:d}'.format(self.id))
        while self.env.now < self.sim.hours or self.sim.ppl_on_course:
            if self.sim.ppl_on_course < 0:
                print('ERROR: negative number of people on course!')
                sys.exit(-1)
            print('{:s} {:d} people on course'.format(hms(self.env.now), self.sim.ppl_on_course))
            for location in [self.sim.lot, self.sim.course]:
                # exit condition kludge:
                if self.env.now >= self.sim.hours and self.sim.ppl_on_course == 0:
                    print('{:s} Bus runs done for today!'.format(hms(self.env.now)))
                    self.sim.stats()
                    sys.exit(0)

                # start log for this run
                run = {'bus_id': self.id, 'start_boarding': self.env.now}

                # board passengers

                req = location.request() # enter location queue
                yield req
                print('{:s} {:d} buses in queue at {:s}'.format(hms(self.env.now), len(location.queue), location.desc))
                departure_time = self.env.now+self.sim.time_interval
                while self.env.now < departure_time and self.passengers < self.capacity:
                    exp_ar = location.exp_ar(self.env.now)
                    # print('{:s} {:10.5f} arrivals per hour expected at {:s}'.format(hms(self.env.now), exp_ar, location.desc))
                    if exp_ar < 0:
                        print('{:s} wtf at {:s}'.format(hms(self.env.now), location.desc))
                    elif exp_ar == 0:
                        # Probably an arrival at course early in the day
                        # Wait it out (kludge)
                        yield self.env.timeout(departure_time-self.env.now)
                        if location == self.sim.course:
                            self.passengers = min(self.capacity, self.sim.ppl_on_course)
                            self.sim.ppl_on_course -= self.passengers
                        elif self.env.now >= self.sim.hours:
                            # TODO: back to course if still ppl on course
                            # otherwise "uncharter" the bus
                            continue
                        else:
                            print('wtaf?')
                    else:
                        iat = rv.exponential(location.exp_ar(self.env.now))[0]
                        # print('{:s} iat={:10.5f}'.format(hms(self.env.now), iat))
                        if self.env.now+iat > departure_time:
                            yield self.env.timeout(departure_time-self.env.now)
                        else:
                            yield self.env.timeout(iat)
                            self.passengers += 1
                            # print('{:s} a passenger boarded bus #{:d} at {:s}'.format(hms(self.env.now), self.id, location.desc))
                            if location == self.sim.course:
                                self.sim.ppl_on_course -= 1
                yield location.release(req) # leave location queue

                # drive to destination

                print('{:s} Bus #{:d} departing from {:s} with {:d} passengers'.format(hms(self.env.now), self.id, location.desc, self.passengers))
                run['passengers'] = self.passengers
                run['depart'] = self.env.now
                yield self.env.timeout(rv.lognormal(log(location.m), location.m/100)[0])

                # disembark

                run['disembark'] = self.env.now
                self.sim.log.append(run)
                dest = self.sim.course if location == self.sim.lot else self.sim.lot
                print('{:s} Bus #{:d} dropping off {:d} passengers at {:s}.'.format(hms(self.env.now), self.id, self.passengers, dest.desc))
                if dest == self.sim.course:
                    self.sim.ppl_on_course += self.passengers
                self.passengers = 0
        print('{:s} Bus #{:d} is done for the day!'.format(hms(self.env.now), self.id))
        self.sim.buses_out -= 1
        if self.sim.buses_out == 0:
            # terminate simulation
            print('{:s} All buses done for day!')
            sys.exit(0)


Round()
